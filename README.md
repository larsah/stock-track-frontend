# README #



### How do I get set up? ###

Important: Out of the box, the app is configured to connect to a stock-track-backend Docker container at port 49160. If you wish to change the target port, head 
over to /app/src/services/stock-data-service.ts and modify const host = "http://localhost:XXXX" accordingly.

To run the app in development mode, getting started is as easy as:

1. Clone the repo
2. Install node modules. Run this command inside /app

        npm install

3. Start the app. Run this command inside /app

        npm start

4. Navigate to http://localhost:3000 and enjoy your AAPL stock prices :)


;) If you want to build a production ready app, run:

        npm run build

