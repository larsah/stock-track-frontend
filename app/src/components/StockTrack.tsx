import StockDataService from '../services/stock-data-service'
import StockChart from './StockChart'

function StockTrack() {
  const stockdataservice: StockDataService = new StockDataService()
  const dataservices = {
    stockdataservice: stockdataservice
  }
  return (
    <div className="App">
      <StockChart>{dataservices}</StockChart>
    </div>
  );
}

export default StockTrack