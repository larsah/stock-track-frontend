import "./StockChart.css"
import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'
import React from 'react'
import StockDataServiceInterface from '../services/stock-data-service-interface'

//Initial Highcharts options. Data is added to the series object inside componentDidMount()
const options = {
  type: 'line',
  rangeSelector: {
    selected: 1
  },
  navigator: {
    enabled: true
  },
  title: {
    text: 'Apple (AAPL) Stock Price - Adjusted'
  },
  series:[{
    name: 'Closing price - USD',
    marker: {
      enabled: true,
      radius: 3
    },
    shadow: true,
    tooltip: {
      valueDecimals: 2
    }
  }]
}

class StockChart extends React.Component<{}, { options: any, loadingData: boolean, message: string }>  {

  private readonly stockdataservice: StockDataServiceInterface;

  constructor(props: any) {
    super(props)
    //Inject service dependency
    this.stockdataservice = props.children.stockdataservice;
    this.state = {
      options: options,
      loadingData: true,
      message: ""
    }
  }

  render() {
    return (
      <div className = "chart">
        <HighchartsReact
          highcharts={Highcharts}
          constructorType={"stockChart"}
          options={this.state.options}></HighchartsReact>
          
          {this.state.loadingData ? 
            <div className = "loading">
              <span>Getting data, please wait...</span>
            </div>: null}
          
          <div className = "message">
            <span>{this.state.message}</span>
          </div>
      </div>
    )
  }


  componentDidMount() {
    this.stockdataservice.fetchDailyDataForStock("AAPL")
      .then((data) => {
        console.log(data)
        this.setState({
          options: {
            series: [{
              data: data, 
            }
            ]
          },
          loadingData: false,
          message: "If data doesn't show up. Try refreshing the site."
        }
        )
      })
      .catch((err) => {
        this.setState({
          loadingData: false,
          message: "Something went wrong. The error is: " + err
        })
      })
  }
}

export default StockChart