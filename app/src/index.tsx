import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import StockTrack from './components/StockTrack';

ReactDOM.render(
  <React.StrictMode>
    <StockTrack />
  </React.StrictMode>,
  document.getElementById('root')
);

