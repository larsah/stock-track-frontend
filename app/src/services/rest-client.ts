import http from 'http'

//A generic rest client that can be extended by more specific ones
export default abstract class RestClient {

    get(requestUrl:string):Promise<any> {
        return new Promise((resolve, reject) => {
            console.log("Making request to: " + requestUrl)

            const request = http.get(requestUrl, (resp) => {
                let body = "";
    
                resp.on("data", (chunk) => {
                    body+=chunk
                })
                
                resp.on("end", () => {
                    console.log("Data fetched")
                    resolve(JSON.parse(body))
                })
    
            }).on("error", (err) => {
                console.log("Error: " + err.message);
                reject(err)
            })

        })
    }



}