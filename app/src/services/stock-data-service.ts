import RestClient from './rest-client'
import StockDataServiceInterface from './stock-data-service-interface'

const host = "http://localhost:49160"
const stockDataPath = "/stock-data"


export default class StockDataService extends RestClient implements StockDataServiceInterface {

    //Fetches stock data for AAPL. If it were required that the app could fetch data for different stocks. The symbol parameter would be useful. It is now useless though.
    fetchDailyDataForStock(symbol: string): Promise<number[][]> {
        const requestUrl = host + stockDataPath
        
        return this.get(requestUrl)   
    }
}