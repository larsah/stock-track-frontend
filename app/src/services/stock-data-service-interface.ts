
//Interface for stock data services. Useful in dependency injection and testing.
export default interface StockDataServiceInterface {
  
    fetchDailyDataForStock(symbol:string): Promise<number[][]>
}